﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(MinhaCasaESuaCasa.Startup))]
namespace MinhaCasaESuaCasa
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
